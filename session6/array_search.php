<?php
$array = array('a' => 'man', 6 =>'cow', '5' => 'goat' );
$search = array_search('goat',$array);
$search2 = array_search('man',$array);
$search3 = array_search('cow',$array);
print_r( $search);
echo "<br>";
print_r( $search2);
echo "<br>";
print_r( $search3);
echo "<br>---------------------------------------------<br>";
$array2 = array('a' => 'man', 5 =>'cow', '5' => 'goat' );
$search3 = array_search('cow',$array2);		// shows nothing because there are two 5s here.
print_r( $search3);
echo "<br>---------------------------------------------<br>";

$array3 = array('a'=> 5, 'b'=> '5');
$search4 = array_search('5', $array3, true);		// shows b because 3rd parameter is true and first parameter is string. If don not use 'true' then shows a. 
$search5 = array_search(5, $array3);				// shows a because 5 is integer.
print_r($search4);
echo "<br>";
print_r($search5);
